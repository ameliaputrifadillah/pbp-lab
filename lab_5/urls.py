from django.urls import path
from .views import index, get_note, update_note, delete_note


urlpatterns = [
    path('', index, name='lab-5'),
    # path('<id>', get_note, name='notes_id'),
    path('notes/<id>', get_note, name='notes/notes_id'),
    path('notes/<id>/update', update_note, name='notes/notes_id/update'),
    path('notes/<id>/delete', delete_note, name='notes/notes_id/delete'),
    
]

