from django.shortcuts import render, HttpResponseRedirect, get_object_or_404
from .forms import NoteForm
from lab_2.models import Note
from django.http.response import HttpResponse
from django.core import serializers

def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab5_index.html', response)

def get_note(request, id):
    note = Note.objects.get(id=id)
    response = {'note': note}
    return render(request, 'lab5_index.html', response)


def update_note(request, id):
    # dictionary for initial data with
    # field names as keys
 
    # fetch the object related to passed id
    obj = get_object_or_404(Note, id = id)
 
    # pass the object as instance in form
    form = NoteForm(request.POST or None, instance = obj)
 
    # save the data from the form and
    # redirect to detail_view
    if form.is_valid():
        form.save()
        return HttpResponseRedirect("/"+id)
 
    # add form dictionary to context
    response = {'form': form}
 
    return render(request, "lab5_index.html", response)


def delete_note(request, id):
    # dictionary for initial data with
    # field names as keys
    response ={}
 
    # fetch the object related to passed id
    obj = get_object_or_404(Note, id = id)
 
 
    if request.method =="POST":
        # delete object
        obj.delete()
        # after deleting redirect to
        # home page
        return HttpResponseRedirect("/")
 
    return render(request, "lab5_index.html", response)