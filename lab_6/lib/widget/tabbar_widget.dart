import 'package:flutter/material.dart';

class TabBarWidget extends StatelessWidget {
  final String title;
  final List<Tab> tabs;
  final List<Widget> children;

  const TabBarWidget({
    Key? key,
    required this.title,
    required this.tabs,
    required this.children,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => DefaultTabController(
        length: tabs.length,
        child: Scaffold(
            body: TabBarView(children: children),
            appBar: AppBar(
              title: Text(title),
              centerTitle: true,
              flexibleSpace: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [Colors.purple.shade300, Colors.blue.shade300],
                    begin: Alignment.bottomRight,
                    end: Alignment.topLeft,
                  ),
                ),
              ),
              titleSpacing: 20,
            ),
            bottomNavigationBar: BottomAppBar(
              color: Colors.purple.shade300,
              child: TabBar(
                padding: const EdgeInsets.symmetric(horizontal: 90),
                isScrollable: true,
                indicatorColor: Colors.white,
                indicatorWeight: 5,
                tabs: tabs,
              ),
            )),
      );
}
