import 'package:flutter/material.dart';
import 'package:lab_6/page/home_page.dart';
import 'package:lab_6/widget/tabbar_widget.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  static const String title = 'Covid-19 App';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // title: title,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Covid-19 App'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) => TabBarWidget(
        title: MyApp.title,
        //        child:Align(
              // alignment: Alignment.center,
        tabs: const [
          Tab(icon: Icon(Icons.home), text: 'Beranda'),
          Tab(icon: Icon(Icons.table_chart), text: 'Tabel Covid'),
          Tab(icon: Icon(Icons.pie_chart), text: 'Statistik Vaksinasi'),
          
        ],
        children: [
          Beranda(),
          Container(),
          Container(),
        ],
      );}
