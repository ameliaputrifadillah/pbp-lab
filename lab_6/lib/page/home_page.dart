import 'package:flutter/material.dart';
import 'package:lab_6/page/home_page_edittable.dart';


class Beranda extends StatefulWidget {
  @override
  State<Beranda> createState() => _BerandaState();
}

class _BerandaState extends State<Beranda> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.all(30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const Text(
              "Apa itu COVID-19?",
              textAlign: TextAlign.left,
              style: TextStyle(
                  letterSpacing: 0.5,
                  fontSize: 16,
                  fontWeight: FontWeight.w700),
            ),
            const Padding(
              padding: EdgeInsets.fromLTRB(0, 20, 50, 20),
              child: Text(
                "COVID-19 adalah penyakit menular yang disebabkan oleh jenis coronavirus yang baru ditemukan. Virus baru dan penyakit yang disebabkannya ini tidak dikenal sebelum mulainya wabah di Wuhan, Tiongkok, bulan Desember 2019. COVID-19 ini sekarang menjadi sebuah pandemi yang terjadi di banyak negara di seluruh dunia.",
                style: TextStyle(letterSpacing: 0.5, fontSize: 14),
              ),
            ),
            const Text(
              "Apa itu Vaksinasi COVID-19?",
              textAlign: TextAlign.right,
              style: TextStyle(
                  letterSpacing: 0.5,
                  fontSize: 16,
                  fontWeight: FontWeight.w700),
            ),
            const Padding(
              padding: EdgeInsets.fromLTRB(50, 20, 0, 20),
              child: Text(
                "Vaksinasi Covid-19 merupakan salah satu upaya pemerintah Indonesia dalam menangani masalah Covid-19. Vaksinasi Covid-19 bertujuan untuk menciptakan kekebalan kelompok (herd immunity) agar masyarakat menjadi lebih produktif dalam menjalankan aktivitas kesehariannya.",
                textAlign: TextAlign.right,
                style: TextStyle(letterSpacing: 0.5, fontSize: 14),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: Image.asset('assets/images/ayo_vaksin.png'),
            ),
            const Padding(
              padding: EdgeInsets.fromLTRB(50, 20, 0, 20),
              child: Text(
                "Tabel Kasus Covid-19",
                textAlign: TextAlign.center,
                style: TextStyle(
                    letterSpacing: 0.5,
                    fontSize: 16,
                    fontWeight: FontWeight.w700),
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: DataTable(
                columns: const <DataColumn>[
                  DataColumn(
                    label: Text(
                      'Bulan',
                      style: TextStyle(fontWeight: FontWeight.w700),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'Tahun',
                      style: TextStyle(fontWeight: FontWeight.w700),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'Kasus Tambahan',
                      style: TextStyle(fontWeight: FontWeight.w700),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'Kasus Kumulatif',
                      style: TextStyle(fontWeight: FontWeight.w700),
                    ),
                  ),
                ],
                rows: const <DataRow>[
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text('Maret')),
                      DataCell(Text('2020')),
                      DataCell(Text('1452')),
                      DataCell(Text('1452')),
                    ],
                  ),
                ],
              ),
            ),
            TextButton(
              style: TextButton.styleFrom(
                primary: Colors.blue,
              ),
              onPressed: () {
                Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => const EditTable()
                )
                );
              },
              child: const Text('edit tabel'),
            )
          ],
        ),
      ),
    );
  }
}
