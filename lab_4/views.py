from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from .forms import NoteForm
from lab_2.models import Note

def index(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}

    form = NoteForm(request.POST or None, request.FILES or None)

    if form.is_valid():
        form.save()
        return HttpResponseRedirect('/lab-4')

    response['form']= form
    return render(request, "lab4_form.html", response)

def note_list(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)