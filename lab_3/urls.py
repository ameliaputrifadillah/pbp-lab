from django.urls import path, include
from .views import index, add_friend


urlpatterns = [
    path('', index, name='lab-3'),
    path('add/', add_friend ,name='add')
]
