from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from .forms import FriendForm
from lab_1.models import Friend


@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all().values()
    response = {'friends': friends}
    return render(request, 'friend_list_lab1.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    friends = Friend.objects.all().values()
    response = {'friends': friends}

    form = FriendForm(request.POST or None, request.FILES or None)

    if form.is_valid():
        form.save()

    response['form']= form
    return render(request, "lab3_form.html", response)