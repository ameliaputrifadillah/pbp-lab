from django.urls import path, include
from .views import index, json, xml


urlpatterns = [
    path('', index, name='lab-2'),
    path('xml/', xml ,name='xml'),
    path('json/', json ,name='json')
]