from django.db import models

class Note(models.Model):
    To = models.CharField(max_length=20)
    From = models.CharField(max_length=20)
    Title = models.CharField(max_length=20)
    Messages = models.CharField(max_length=300)